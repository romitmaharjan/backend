const mongoose = require('mongoose')
const Schema = mongoose.Schema

const adminSchema = new Schema({
    isActive: {
        type: Boolean,
        default: false
    },
    email: String,
    password: String
})

const model = mongoose.model('Admin', adminSchema)

module.exports = model