const Admin = require('../models/admin')
const bcrypt = require('bcrypt')
const passport = require('passport')
const jwt = require('jsonwebtoken')
require('dotenv').config()

const createAdmin = (req, res) => {
    const userData = {
        email: req.body.email,
        password: req.body.password,
        isActive: req.body.isActive
    }

    Admin.findOne({
        email: req.body.email
    })
    .then(user => {
        if(!user){
            bcrypt.hash(req.body.password, 10, (err, hash) =>{
                userData.password = hash

                Admin.create(userData)
                .then(result => {
                    return res.send(result)
                })
                .catch(err => console.log(err))
            })   
        } else {
            console.log("Already used Email Address")
        }
    })
}

const login = (req, res) => {
    Admin.findOne({
        email: req.body.email
    })
    .then(user => {
        if(user == null){
            return res.status(400).send({error: "Email not found"})
        } else {        
        if(user.isActive){
            bcrypt.compare(req.body.password, user.password, (err, result) => {
                if(result){                    
                    
                    const email = req.body.email
                    const user = {name: email}

                    const accessToken = jwt.sign(user, process.env.accessToken, {expiresIn: '1h'})
                    res.json({accessToken:accessToken})            
                } else {
                    return res.status(401).send({error: "Incorrect password"})
                }
            })           
        } else {
            return res.status(402).send({error: "User is not active"})
        }
    }
    })
    .catch(err => console.log(err))
}

module.exports = {createAdmin, login}
