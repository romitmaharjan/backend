const Event = require('../models/event');
const json2csvparser = require("json2csv").Parser;
const fs = require("fs");

const eventRegistration = async (req, res) => {
    const registration = {
        eventTitle: req.body.eventTitle,
        eventDate: req.body.eventDate,
        eventTime: req.body.eventTime,
        attributes: req.body.newList
    }
    Event.create(registration)
    .then(result => {
        return res.send(result)
    })
    .catch(err => {
        return res.send(err)
    })
}

const getRegistrationDetails = async (req, res) => {
    Event.find()
    .then(registration => {
        return res.send(registration)
    })
    .catch(err => {
        console.log(err)
        return res.send(err)
    })
}

const getEventRegistrationAttributes = (req, res) => {
    Event.findById({_id: req.params.id})
    .then(attributes => {
        console.log(attributes.eventTitle)
        return res.send(attributes)
    })
    .catch(err => {        
        return res.status(402).send({error: "Invalid Event ID"})      
    })
}

const downloadParticipantsDetails = (req, res) => {
    Event.findById({_id: req.params.id})
    .then(attributes => {
        /*const fields = [];
        const data = attributes.participants[0]; 
        const fieldsFromDB = Object.keys(data)
        const eventTitle=attributes.eventTitle;
        const eventDate = attributes.eventDate;   
        const title = eventTitle + "-Details.csv";                         
        for(var x = 0; x < fieldsFromDB.length; x++){
            fields.push(fieldsFromDB[x]);
        }       
        const newjson2csv = new json2csvparser({fields}); 
        const csvData = newjson2csv.parse(attributes.participants);
        /*fs.writeFile(title, csvData, function(error){
            if(error) throw error;                  
        }) 
        res.header('Content-Type', 'text/csv');
        res.attachment(title);
        return res.send(csvData);*/
    })
    .catch(err => {        
        return res.status(402).send({error: "Invalid Event ID"})      
    })
}

const addParticipants = (req, res) => {
    const newParticipant = req.body.participant
    Event.findByIdAndUpdate({_id: req.body.id},
        {$push: {
            "participants" : newParticipant}}
        )
        .then(result => {
            return res.send(result)
        })
        .catch(err => {
            return res.send(err)
        })
}

module.exports = {
    eventRegistration,
    getRegistrationDetails,
    getEventRegistrationAttributes, 
    addParticipants,
    downloadParticipantsDetails
}