const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const Student = require('../models/student')
const {addStudent, allStudents, deleteStudent, editStudent, getStudent} = require("../controllers/student-controller")
const {createAdmin, login} = require('../controllers/admin-controller')
const {eventRegistration, getRegistrationDetails, getEventRegistrationAttributes, addParticipants, downloadParticipantsDetails} = require('../controllers/event-controller')
const { feedback } = require('../controllers/feedback-controller')

function authenticateToken(req, res, next) {  
  const authHeader = req.headers.authorization
  const token = authHeader && authHeader.split(' ')[1]
  if(token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.accessToken, (err, user) => {      
      if(err) return res.status(403).send({error:"Token expired"})
      req.user = user
      next()
  })
}

router.post('/addStudent', authenticateToken, addStudent)
router.get('/allStudents', allStudents)
router.delete('/deleteStudent', deleteStudent)
router.put('/updateStudent', editStudent)
router.get('/getStudent/:id', authenticateToken, getStudent)
router.post('/createAdmin', createAdmin)
router.post('/login', login)
router.post('/eventRegistration', eventRegistration)
router.get('/getRegistrationDetails', getRegistrationDetails)
router.post('/feedback', feedback)
router.get('/getEventRegistrationAttributes/:id', getEventRegistrationAttributes)
router.get('/downloadParticipantsDetails/:id', downloadParticipantsDetails)
router.put('/addParticipants', addParticipants)
module.exports = router;