const express = require('express')
const cors = require('cors')
const passport = require('passport')
const session = require('express-session')

const app = express()
app.use(cors())
app.use(express.json())
app.use(session({
    secret: 'keyboard cat',
     resave: false,
     saveUninitialized: true,
     cookie: { secure: true }
}))

app.use(passport.initialize());
app.use(passport.session());
app.use(require('./routes/index'))

module.exports = app